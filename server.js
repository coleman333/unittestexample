const http = require('http');

http.createServer((req, res, next) => {
	// req.body.user
	switch (req.url) {
		case '/':
			return res.end('hello');
		case '/users':
			return res.end('world');
		default:
			return res.end('default')
	}
}).listen(3010);