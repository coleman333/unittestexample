const expect = require('chai').expect;
const axios = require('axios');

// AWS: S3, EC2, security groups, lambdas, load balancer,  //ssl, domains.
// lets encrypt + nginx
// + Rancher + docker-compose, + CI/CD.
// unit, e2e

describe('Server', function () {

	it('Should return "hello"', async function () {
		const result = await axios({
			method: 'post',
			url: 'http://localhost:3010',
		});
		expect(result.data).to.be.equal('hello');
	});

	it('Should return "world"', async function () {
		const result = await axios({
			method: 'post',
			url: 'http://localhost:3010/users',
		});
		expect(result.data).to.be.equal('world');
	});

	it('Should return status ECONNREFUSED', async function () {
		try {
			const result = await axios({
				method: 'post',
				url: 'http://localhost:3011/users',
			});
			expect(result).to.be.null;
		} catch (e) {

			expect(e.code).to.be.equal('ECONNREFUSED');
		}
	});

});