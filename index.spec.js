const expect = require('chai').expect;
const Calculator = require('./index');

// AWS: S3, EC2, security groups, lambdas, ssl, domains.
// + Rancher + docker-compose, + CI/CD.
// unit, e2e

describe('Calculator', function () {

	before(function () {

	});
	after(function () {

	});

	const calc = new Calculator();

	it('Should add two numbers', function () {

		for (let i = 0; i < 100; i++) {
			for (let j = 100; j > 0; j--) {
				expect(calc.add(i, j)).to.be.equal(i + j);
			}
		}

	});

	it('Should\'t add two strings', function () {
		expect(calc.add(2, '3')).to.be.NaN;
	});

	it('Should\'t add two objects', function () {
		expect(calc.add({value: 2}, [1, 2])).to.be.NaN;
	});


});